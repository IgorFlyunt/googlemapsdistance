﻿using System;
using System.Collections.Generic;
using Distance.Models;
using Distance.Service;
using NUnit.Framework;

namespace Distance.UnitTests
{
    [TestFixture]
    public class DistancePrinterTests
    {
        [SetUp]
        public void SetUp()
        {
            _list = new List<Element>();
            var element = new Element();
            element.distance = new Models.Distance();
            element.duration = new Duration();
            element.distance.value = 5000;
            element.distance.text = "5 km";
            element.duration.value = 5000;
            element.duration.text = " 1 hour 10 min";
            _list.Add(element);
        }

        private IDistancePrinter _distancePrinter;
        private List<Element> _list;

        [Test]
        public void DistancePrinter_TestConsoleElements()
        {
            try
            {
                _distancePrinter = new ConsoleDistancePrinter();
                _distancePrinter.DisplayElements(_list);
            }
            catch (Exception)
            {
                Assert.Fail("Exception on console element displaying");
            }
        }

        [Test]
        public void DistancePrinter_TestConsoleNullElements_ThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() =>
            {
                _distancePrinter = new ConsoleDistancePrinter();
                _distancePrinter.DisplayElements(null);
            });
        }

        [Test]
        public void DistancePrinter_TestDebugElements()
        {
            try
            {
                _distancePrinter = new DebugDistancePrinter();
                _distancePrinter.DisplayElements(_list);
            }
            catch (Exception)
            {
                Assert.Fail("Exception on debug element displaying");
            }
        }

        [Test]
        public void DistancePrinter_TestDebugNullElements_ThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() =>
            {
                _distancePrinter = new DebugDistancePrinter();
                _distancePrinter.DisplayElements(null);
            });
        }
    }
}