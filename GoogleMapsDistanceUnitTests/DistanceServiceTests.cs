﻿using System;
using Distance.Service;
using NUnit.Framework;

namespace Distance.UnitTests
{
    [TestFixture]
    public class DistanceServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _apiKey = "AIzaSyDiGbhm3Y9PFY3AYsgGq06khNzBDh5rBHs";
            _originAddress = "Lviv, Ukraine";
            _destinationAddress = "Kyiv, Ukraine";
            _url = string.Format(
                $"https://maps.googleapis.com/maps/api/distancematrix/json?origins={_originAddress}&destinations={_destinationAddress}&key={_apiKey}");
            _distanceService = new DistanceService(_originAddress, _destinationAddress);
        }

        private DistanceService _distanceService;
        private string _apiKey;
        private string _originAddress;
        private string _destinationAddress;
        private string _url;

        [Test]
        public void GetDistanceFromGoogle_InvalidApiKey_ThrowException()
        {
            DistanceService.ApiKey = null;
            Assert.Throws<NullReferenceException>(() => { _distanceService.GetDistanceFromGoogle(); });
        }

        [Test]
        public void GetDistanceFromGoogle_CheckUrlRequestToGoogle()
        {
            Assert.IsTrue(_url.Contains(_originAddress));
            Assert.IsTrue(_url.Contains(_destinationAddress));
            Assert.IsTrue(_url.Contains(_apiKey));
        }

        [Test]
        public void GetDistanceFromGoogle_CheckDistanceResponse_DistanceIsGreaterThanZero()
        {
            var result = _distanceService.GetDistanceFromGoogle();
            if (result != null)
            {
                Assert.IsTrue(result.Count > 0);
                foreach (var item in result) Assert.IsTrue(item.distance.value > 0);
            }
            else
            {
                Assert.Fail("Request from Google is null");
            }
        }

        [Test]
        public void GetDistanceFromGoogle_CheckDurationResponse_DurationIsGreaterThanZero()
        {
            var result = _distanceService.GetDistanceFromGoogle();
            if (result != null)
            {
                Assert.IsTrue(result.Count > 0);
                foreach (var item in result) Assert.IsTrue(item.duration.value > 0);
            }
            else
            {
                Assert.Fail("Request from Google is null");
            }
        }
    }
}