﻿using System;
using Distance.Service;

namespace Distance.Console
{
    public static class EnterAddress
    {
        private static IDistancePrinter printer;

        public static void SetAddress()
        {
            try
            {
                System.Console.WriteLine("Enter Origin Address :");
                var originAddress = System.Console.ReadLine();
                System.Console.WriteLine("Enter Destination Address :");
                var destinationAddress = System.Console.ReadLine();
                if (string.IsNullOrEmpty(originAddress) || string.IsNullOrEmpty(destinationAddress))
                {
                    System.Console.WriteLine("Address is empty, restart the program, and enter address correctly !");
                }
                else
                {
                    var distanceService = new DistanceService(originAddress, destinationAddress);
                    var elements = distanceService.GetDistanceFromGoogle();
                    System.Console.WriteLine("Enter output type :\n1 - Console\n2 - Debug");
                    var outputType = Convert.ToInt32(System.Console.ReadLine());
                    if (outputType == 1)
                    {
                        printer = new ConsoleDistancePrinter();
                        printer.DisplayElements(elements);
                    }
                    else if (outputType == 2)
                    {
                        printer = new DebugDistancePrinter();
                        printer.DisplayElements(elements);
                    }
                    else
                    {
                        throw new Exception("Invalid output type !");
                    }
                }
            }
            catch (Exception m)
            {
                throw new Exception(m.Message);
            }
        }
    }
}