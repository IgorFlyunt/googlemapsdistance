﻿using Distance.Models;
using System.Collections.Generic;
namespace Distance.Service
{
    public interface IDistancePrinter
    {
        public void DisplayElements(IEnumerable<Element> elements);
    }
}
