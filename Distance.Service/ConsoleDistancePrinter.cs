﻿using Distance.Models;
using System;
using System.Collections.Generic;
namespace Distance.Service
{
    public class ConsoleDistancePrinter : IDistancePrinter
    {
        public void DisplayElements(IEnumerable<Element> elements)
        {
            foreach (var item in elements)
            {
                if (item.distance != null)
                {
                    Console.WriteLine("Distance in meter " + item.distance.value + " m");
                    Console.WriteLine("Distance in kilometer :" + item.distance.text);
                    Console.WriteLine("Distance in miles : " + Math.Round((double)item.distance.value / 1609) + " mi");
                    Console.WriteLine("Average trip duration : " + item.duration.text);
                }
                else
                {
                    Console.WriteLine("Response from google is null - please investigate response");
                }
            }
        }                
    }
}
