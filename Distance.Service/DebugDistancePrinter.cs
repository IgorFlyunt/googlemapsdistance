﻿using Distance.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace Distance.Service
{
    public class DebugDistancePrinter : IDistancePrinter
    {
        public void DisplayElements(IEnumerable<Element> elements)
        {
            
            foreach (var item in elements)
            {
                if (item.distance != null)
                {
                    Debug.WriteLine("Distance in meter " + item.distance.value + " m");
                    Debug.WriteLine("Distance in kilometer :" + item.distance.text);
                    Debug.WriteLine("Distance in miles : " + Math.Round((double)item.distance.value / 1609) + " mi");
                    Debug.WriteLine("Average trip duration : " + item.duration.text);
                }
                else
                {
                    Debug.WriteLine("Response from google is null - please investigate response");
                }
            }
        }
    }
}
