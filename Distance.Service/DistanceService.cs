﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Distance.Models;
using Newtonsoft.Json;
namespace Distance.Service
{
    public class DistanceService
    {
        private int myVar;

        public int MyProperty
        {
            get { return myVar; }
            set { myVar = value; }
        }



        public static string ApiKey = "AIzaSyDiGbhm3Y9PFY3AYsgGq06khNzBDh5rBHs";
        private string OriginAddress { get; set; }
        private string DestinationAddress { get; set; }
        private string Url => $"https://maps.googleapis.com/maps/api/distancematrix/json?origins={OriginAddress}&destinations={DestinationAddress}&key={ApiKey}";

        public DistanceService(string originAddress, string destinationAddress)
        {
            OriginAddress = originAddress;
            DestinationAddress = destinationAddress;
        }
        public List<Element> GetDistanceFromGoogle()
        {
            if (string.IsNullOrEmpty((ApiKey)))
            {
                throw new NullReferenceException("No valid maps API key");
            }
            try
            {
                using (var webClient = new WebClient())
                {
                    using (StreamReader streamReader = new StreamReader(webClient.OpenRead(Url)))
                    {
                        var responseFromGoogle = streamReader.ReadToEnd();
                        List<Element> list = new List<Element>();
                        DistanceResponse distanceResponse = JsonConvert.DeserializeObject<DistanceResponse>(responseFromGoogle);
                        if (distanceResponse?.rows == null) return list;
                        // the same  if (distanceResponse == null || && rows == null)
                        foreach (var responseList in distanceResponse.rows)
                        {
                            if (distanceResponse.rows == null || !distanceResponse.rows.Any()) continue;
                            foreach (var element in responseList.elements)
                            {
                                list.Add(element);
                            }
                        }
                        return list;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error :" + ex.Message);
            }
        }
    }
}
