﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Distance.Models
{
    /// <summary>
    /// Classes for deserialize Json response from Google Maps MatrixAPI
    /// </summary>
    public class Distance
    {
       
        public string text { get; set; }
        public int value { get; set; }
    }
    public class Duration
    {
        public string text { get; set; }
        public int value { get; set; }
    }
    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }
    public class DistanceResponseList
    {
        public List<Element> elements { get; set; }
    }
    public class DistanceResponse
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<DistanceResponseList> rows { get; set; }
        public string status { get; set; }
    }
}
